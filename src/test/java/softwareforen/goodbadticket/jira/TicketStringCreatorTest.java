package softwareforen.goodbadticket.jira;

import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;

public class TicketStringCreatorTest {

	@Test
	public void testCreateTicketList_nullValues() {
		List<String> ticketNumbers = TicketStringCreator.createTicketList(null,
				null);
		assertTrue(ticketNumbers.isEmpty());
	}

	@Test
	public void testCreateTicketList_nullProject() {
		List<String> ticketNumbers = TicketStringCreator.createTicketList(null,
				"123");
		assertTrue(ticketNumbers.isEmpty());
	}

	@Test
	public void testCreateTicketList_emptyProject() {
		List<String> ticketNumbers = TicketStringCreator.createTicketList("",
				"123");
		assertTrue(ticketNumbers.isEmpty());
	}

	@Test
	public void testCreateTicketList_nullNumbers() {
		List<String> ticketNumbers = TicketStringCreator.createTicketList(
				"STR", null);
		assertTrue(ticketNumbers.isEmpty());
	}

	@Test
	public void testCreateTicketList_emptyNumbers() {
		List<String> ticketNumbers = TicketStringCreator.createTicketList(
				"STR", "");
		assertTrue(ticketNumbers.isEmpty());
	}

	@Test
	public void testCreateTicketList_oneNumber() {
		List<String> ticketNumbers = TicketStringCreator.createTicketList(
				"STR", "213");
		assertEquals(1, ticketNumbers.size());
		assertEquals("STR-213", ticketNumbers.get(0));
	}

	@Test
	public void testCreateTicketList_twoNumbers() {
		List<String> ticketNumbers = TicketStringCreator.createTicketList(
				"STR", "213, 561");
		assertEquals(2, ticketNumbers.size());
		assertEquals("STR-213", ticketNumbers.get(0));
		assertEquals("STR-561", ticketNumbers.get(1));
	}

	@Test
	public void testCreateTicketList_twoNumbersWhitespace() {
		List<String> ticketNumbers = TicketStringCreator.createTicketList(
				"STR", "213 561");
		assertEquals(2, ticketNumbers.size());
		assertEquals("STR-213", ticketNumbers.get(0));
		assertEquals("STR-561", ticketNumbers.get(1));
	}

	@Test
	public void testCreateTicketList_twoNumbersTwoWhitespaces() {
		List<String> ticketNumbers = TicketStringCreator.createTicketList(
				"STR", "213  561");
		assertEquals(2, ticketNumbers.size());
		assertEquals("STR-213", ticketNumbers.get(0));
		assertEquals("STR-561", ticketNumbers.get(1));
	}

	@Test
	public void testCreateTicketList_twoNumbersCommaAndTwoWhitespaces() {
		List<String> ticketNumbers = TicketStringCreator.createTicketList(
				"STR", "213 ,  561");
		assertEquals(2, ticketNumbers.size());
		assertEquals("STR-213", ticketNumbers.get(0));
		assertEquals("STR-561", ticketNumbers.get(1));
	}

	@Test
	public void testCreateTicketList_numberRange() {
		List<String> ticketNumbers = TicketStringCreator.createTicketList(
				"STR", "213-215");
		assertEquals(3, ticketNumbers.size());
		assertEquals("STR-213", ticketNumbers.get(0));
		assertEquals("STR-214", ticketNumbers.get(1));
		assertEquals("STR-215", ticketNumbers.get(2));
	}

	@Test
	public void testCreateTicketList_numberRangeBigToSmall() {
		List<String> ticketNumbers = TicketStringCreator.createTicketList(
				"STR", "215-213");
		assertEquals(0, ticketNumbers.size());
	}

	@Test
	public void testCreateTicketList_numberRangeAndNormalNumbers() {
		List<String> ticketNumbers = TicketStringCreator.createTicketList(
				"STR", "212, 213-215, 216");
		assertEquals(5, ticketNumbers.size());
		assertEquals("STR-212", ticketNumbers.get(0));
		assertEquals("STR-213", ticketNumbers.get(1));
		assertEquals("STR-214", ticketNumbers.get(2));
		assertEquals("STR-215", ticketNumbers.get(3));
		assertEquals("STR-216", ticketNumbers.get(4));
	}
}
