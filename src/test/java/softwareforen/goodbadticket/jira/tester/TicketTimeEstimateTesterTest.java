package softwareforen.goodbadticket.jira.tester;

import softwareforen.goodbadticket.jira.tester.TicketTimeEstimateTester;
import com.atlassian.jira.rest.client.domain.Issue;
import org.junit.Assert;
import org.junit.Test;
import softwareforen.goodbadticket.jira.data.Status;
import softwareforen.goodbadticket.jira.data.Ticket;

public class TicketTimeEstimateTesterTest {

	@Test
	public void testGood1() {
		Issue issue = TestUtils.createTestIssue(239);
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketTimeEstimateTester.test(ticket);
		Assert.assertEquals(Status.GOOD, ticket.getResult().getStatus());
	}

	@Test
	public void testGood2() {
		Issue issue = TestUtils.createTestIssue(240);
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketTimeEstimateTester.test(ticket);
		Assert.assertEquals(Status.GOOD, ticket.getResult().getStatus());
	}

	@Test
	public void testBad() {
		Issue issue = TestUtils.createTestIssue(241);
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketTimeEstimateTester.test(ticket);
		Assert.assertEquals(Status.BAD, ticket.getResult().getStatus());
	}

	@Test
	public void testBad_noEstimate() {
		Issue issue = TestUtils.createTestIssue(0);
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketTimeEstimateTester.test(ticket);
		Assert.assertEquals(Status.BAD, ticket.getResult().getStatus());
	}
}
