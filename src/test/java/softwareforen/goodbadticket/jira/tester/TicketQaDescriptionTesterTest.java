package softwareforen.goodbadticket.jira.tester;

import com.atlassian.jira.rest.client.domain.Issue;
import org.junit.Assert;
import org.junit.Test;
import softwareforen.goodbadticket.jira.data.Status;
import softwareforen.goodbadticket.jira.data.Ticket;

public class TicketQaDescriptionTesterTest {
	@Test
	public void testUknown_noComments() {
		Issue issue = TestUtils.createTestIssueWithComments();
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketQaDescriptionTester.test(ticket);
		Assert.assertEquals(Status.UKNOWN, ticket.getResult().getStatus());
	}

	@Test
	public void testUknown_noQAComments() {
		Issue issue = TestUtils.createTestIssueWithComments(
				"- Was hab ich hier?", "- Was hab ich hier noch?");
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketQaDescriptionTester.test(ticket);
		Assert.assertEquals(Status.UKNOWN, ticket.getResult().getStatus());
	}

	@Test
	public void testGood_qaComments2() {
		Issue issue = TestUtils.createTestIssueWithComments("h3. Hinweis qa",
				"- Was hab ich hier?");
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketQaDescriptionTester.test(ticket);
		Assert.assertEquals(Status.GOOD, ticket.getResult().getStatus());
	}

	@Test
	public void testGood_qaComments3() {
		Issue issue = TestUtils.createTestIssueWithComments("QA-TODO",
				"- Was hab ich hier?");
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketQaDescriptionTester.test(ticket);
		Assert.assertEquals(Status.GOOD, ticket.getResult().getStatus());
	}

	@Test
	public void testGood_qaComments4() {
		Issue issue = TestUtils.createTestIssueWithComments("h3. qa Hinweis",
				"- Was hab ich hier?");
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketQaDescriptionTester.test(ticket);
		Assert.assertEquals(Status.GOOD, ticket.getResult().getStatus());
	}

	@Test
	public void testGood_qaComments5() {
		Issue issue = TestUtils.createTestIssueWithComments("für die qa",
				"- Was hab ich hier?");
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketQaDescriptionTester.test(ticket);
		Assert.assertEquals(Status.GOOD, ticket.getResult().getStatus());
	}

	@Test
	public void testGood_qaCommentsWhitespace() {
		Issue issue = TestUtils.createTestIssueWithComments("h3. qa  Hinweis",
				"- Was hab ich hier?");
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketQaDescriptionTester.test(ticket);
		Assert.assertEquals(Status.GOOD, ticket.getResult().getStatus());
	}

	@Test
	public void testGood_qaCommentsQaAsDescription() {
		Issue issue = TestUtils.createTestIssueWithComments("h3. qa: ",
				"- Was hab ich hier?");
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketQaDescriptionTester.test(ticket);
		Assert.assertEquals(Status.GOOD, ticket.getResult().getStatus());
	}

	@Test
	public void testGood_qaCommentsQaAsDescription2() {
		Issue issue = TestUtils.createTestIssueWithComments(
				"h3. qa \n teste alles", "- Was hab ich hier?");
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketQaDescriptionTester.test(ticket);
		Assert.assertEquals(Status.GOOD, ticket.getResult().getStatus());
	}

	@Test
	public void testBad_qaCommentsQaOnly() {
		Issue issue = TestUtils.createTestIssueWithComments(
				"h3. qa: \n-alles gut \nQA erfolgreich", "- Was hab ich hier?");
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketQaDescriptionTester.test(ticket);
		Assert.assertEquals(Status.UKNOWN, ticket.getResult().getStatus());
	}

	@Test
	public void testUknown_qaCommentsQaOnly2() {
		Issue issue = TestUtils.createTestIssueWithComments(
				"h3. qa \n QA erfolgreich", "- Was hab ich hier?");
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketQaDescriptionTester.test(ticket);
		Assert.assertEquals(Status.UKNOWN, ticket.getResult().getStatus());
	}

	@Test
	public void testGood_qaCommentsNoQa() {
		Issue issue = TestUtils.createTestIssueWithComments("h3. keine qa",
				"- Was hab ich hier?");
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketQaDescriptionTester.test(ticket);
		Assert.assertEquals(Status.GOOD, ticket.getResult().getStatus());
	}

	@Test
	public void testGood_qaCommentsNoQa2() {
		Issue issue = TestUtils.createTestIssueWithComments(
				"h3.qa  nicht erforderlich", "- Was hab ich hier?");
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketQaDescriptionTester.test(ticket);
		Assert.assertEquals(Status.GOOD, ticket.getResult().getStatus());
	}

	@Test
	public void testGood_QaParentTicket() {
		Issue issue = TestUtils
				.createTestIssueWithComments("qa am oberticket\n ");
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketQaTester.test(ticket);
		Assert.assertEquals(Status.GOOD, ticket.getResult().getStatus());
	}

	@Test
	public void testGood_QaParentTicket2() {
		Issue issue = TestUtils
				.createTestIssueWithComments("QA mit STR-4911\n ");
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketQaTester.test(ticket);
		Assert.assertEquals(Status.GOOD, ticket.getResult().getStatus());
	}
}
