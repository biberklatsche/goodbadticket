package softwareforen.goodbadticket.jira.tester;

import softwareforen.goodbadticket.jira.tester.TicketTimeSpentTester;
import com.atlassian.jira.rest.client.domain.Issue;
import org.junit.Assert;
import org.junit.Test;
import softwareforen.goodbadticket.jira.data.Status;
import softwareforen.goodbadticket.jira.data.Ticket;

public class TicketTimeSpentTesterTest {
	@Test
	public void testGood1() {
		Issue issue = TestUtils.createTestIssue(100, 119);
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketTimeSpentTester.test(ticket);
		Assert.assertEquals(Status.GOOD, ticket.getResult().getStatus());
	}

	@Test
	public void testGood2() {
		Issue issue = TestUtils.createTestIssue(100, 120);
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketTimeSpentTester.test(ticket);
		Assert.assertEquals(Status.GOOD, ticket.getResult().getStatus());
	}

	@Test
	public void testBad() {
		Issue issue = TestUtils.createTestIssue(100, 121);
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketTimeSpentTester.test(ticket);
		Assert.assertEquals(Status.BAD, ticket.getResult().getStatus());
	}

	@Test
	public void testBad_noTimeSpent() {
		Issue issue = TestUtils.createTestIssue(100, 0);
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketTimeSpentTester.test(ticket);
		Assert.assertEquals(Status.BAD, ticket.getResult().getStatus());
	}

	@Test
	public void testGood_NoEstimat2() {
		Issue issue = TestUtils.createTestIssue(0, 119);
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketTimeSpentTester.test(ticket);
		Assert.assertEquals(Status.GOOD, ticket.getResult().getStatus());
	}
}
