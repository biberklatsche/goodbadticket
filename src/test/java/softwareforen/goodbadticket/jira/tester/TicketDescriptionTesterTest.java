package softwareforen.goodbadticket.jira.tester;

import com.atlassian.jira.rest.client.domain.Issue;
import org.junit.Assert;
import org.junit.Test;
import softwareforen.goodbadticket.jira.data.Status;
import softwareforen.goodbadticket.jira.data.Ticket;

public class TicketDescriptionTesterTest {
	@Test
	public void testBad_null() {
		Issue issue = TestUtils.createTestIssueWithDescription(null);
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketDescriptionTester.test(ticket);
		Assert.assertEquals(Status.GOOD, ticket.getResult().getStatus());
	}

	@Test
	public void testBad_emptyString() {
		Issue issue = TestUtils.createTestIssueWithDescription("");
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketDescriptionTester.test(ticket);
		Assert.assertEquals(Status.GOOD, ticket.getResult().getStatus());
	}

	@Test
	public void testGood() {
		Issue issue = TestUtils.createTestIssueWithDescription("sdfsdf");
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketDescriptionTester.test(ticket);
		Assert.assertEquals(Status.GOOD, ticket.getResult().getStatus());
	}

	@Test
	public void testGoodNoQa() {
		Issue issue = TestUtils.createTestIssueWithDescription("kein QA");
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketDescriptionTester.test(ticket);
		Assert.assertEquals(Status.GOOD, ticket.getResult().getStatus());
	}

}
