package softwareforen.goodbadticket.jira.tester;

import com.atlassian.jira.rest.client.domain.Issue;
import org.junit.Assert;
import org.junit.Test;
import softwareforen.goodbadticket.jira.data.Status;
import softwareforen.goodbadticket.jira.data.Ticket;

public class TicketStatusTesterTest {
	@Test
	public void testBad_null() {
		Issue issue = TestUtils.createTestIssueWithStatus(null);
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketStatusTester.test(ticket);
		Assert.assertEquals(Status.BAD, ticket.getResult().getStatus());
	}

	@Test
	public void testBad_emptyString() {
		Issue issue = TestUtils.createTestIssueWithStatus("");
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketStatusTester.test(ticket);
		Assert.assertEquals(Status.BAD, ticket.getResult().getStatus());
	}

	@Test
	public void testBad_wrongString() {
		Issue issue = TestUtils.createTestIssueWithStatus("wait");
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketStatusTester.test(ticket);
		Assert.assertEquals(Status.BAD, ticket.getResult().getStatus());
	}

	@Test
	public void testGood() {
		Issue issue = TestUtils.createTestIssueWithStatus("fertig");
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketStatusTester.test(ticket);
		Assert.assertEquals(Status.GOOD, ticket.getResult().getStatus());
	}

}
