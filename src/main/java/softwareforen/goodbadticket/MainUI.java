/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package softwareforen.goodbadticket;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.data.Container.Filter;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.event.FieldEvents;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import softwareforen.goodbadticket.jira.Jira;
import softwareforen.goodbadticket.jira.data.Results;
import softwareforen.goodbadticket.jira.data.Status;
import softwareforen.goodbadticket.jira.data.TicketResult;
import softwareforen.goodbadticket.jira.TicketStringCreator;

/**
 * Kleine dreckige VaadinUI Klasse.
 * 
 * @author lars-wolfram
 */
@Title("GoogBadTicket")
@Theme("custom")
public class MainUI extends UI {

	private final Table ticketTable = new Table();
	private final Label statisticLabel = new Label();
	private Filter ticketFilter;
	private Filter statusFilter;
	private final VerticalLayout ticketLineLayout = new VerticalLayout();

	private Jira jira;
	private BeanItemContainer<TicketResult> ticketContainer = new BeanItemContainer<TicketResult>(
			TicketResult.class);

	protected void init(VaadinRequest request) {
		try {
			initLayout();
			initContactList();
			jira = new Jira();
		} catch (URISyntaxException ex) {
			Notification.show("Jira-Fehler", "Die Url zum Jira ist falsch.",
					Notification.Type.WARNING_MESSAGE);
		}
	}

	private void initLayout() {
		Component ticketPanel = createTicketInputLayout();
		Component filterPanel = createFilterLayout();
		VerticalLayout mainLayout = new VerticalLayout();
		mainLayout.setSizeFull();
		mainLayout.addComponent(ticketPanel);
		mainLayout.addComponent(filterPanel);
		mainLayout.addComponent(ticketTable);
		mainLayout.setExpandRatio(ticketTable, 1);
		ticketTable.setSizeFull();
		this.setContent(mainLayout);

	}

	private Panel createFilterLayout() {
		TextField ticketFilterTextField = new TextField("Ticket");
		ticketFilterTextField
				.addTextChangeListener(new FieldEvents.TextChangeListener() {

					public void textChange(FieldEvents.TextChangeEvent event) {
						ticketContainer.removeContainerFilter(ticketFilter);
						if (!event.getText().isEmpty()) {
							ticketFilter = new SimpleStringFilter(
									"ticketNumber", event.getText(), true,
									false);
							ticketContainer.addContainerFilter(ticketFilter);
						}
					}
				});
		ComboBox statusFilterField = new ComboBox("Status");
		for (Status status : Status.values()) {
			statusFilterField.addItem(status);
		}
		statusFilterField.setNullSelectionAllowed(true);
		statusFilterField
				.addValueChangeListener(new Property.ValueChangeListener() {
					public void valueChange(Property.ValueChangeEvent event) {
						ticketContainer.removeContainerFilter(statusFilter);
						if (event.getProperty().getValue() != null) {
							Status status = (Status) event.getProperty()
									.getValue();
							statusFilter = new SimpleStringFilter("status",
									status.toString(), false, true);
							ticketContainer.addContainerFilter(statusFilter);
						}

					}
				});
		HorizontalLayout filterLayout = new HorizontalLayout();
		filterLayout.addComponent(ticketFilterTextField);
		filterLayout.addComponent(statusFilterField);
		filterLayout.addComponent(statisticLabel);
		filterLayout.setMargin(true);
		filterLayout.setSpacing(true);
		return new Panel(filterLayout);
	}

	private void initContactList() {
		ticketTable.setContainerDataSource(ticketContainer);

		ticketTable.addGeneratedColumn("ticket", new Table.ColumnGenerator() {

			public Object generateCell(Table source, Object itemId,
					Object columnId) {
				TicketResult ticket = (TicketResult) itemId;
				if (ticket.getLink().isEmpty()) {
					return ticket.getTicketNumber();
				} else {
					Link link = new Link(ticket.getTicketNumber(),
							new ExternalResource(ticket.getLink()));
					link.setTargetName("_blank");
					return link;
				}
			}
		});
		ticketTable.setVisibleColumns(new Object[] { "ticket", "status",
				"errorDescription" });
		ticketTable
				.setColumnHeaders(new String[] { "Ticket", "Status", "Grund" });

		ticketTable.setSelectable(false);
		ticketTable.setImmediate(true);
		ticketTable.setMultiSelect(true);
		ticketTable.setSortEnabled(false);

		ticketTable.setCellStyleGenerator(new Table.CellStyleGenerator() {

			public String getStyle(Table source, Object itemId,
					Object propertyId) {
				if (propertyId == null) {
					// Styling for row
					TicketResult ticket = (TicketResult) itemId;
					switch (ticket.getStatus()) {
					case BAD:
						return "highlight-red";
					case GOOD:
						return "highlight-green";
					case UKNOWN:
						return "highlight-yellow";
					default:
						return "";
					}
				} else {
					// styling for column propertyId
					return null;
				}
			}

		});
	}

	private Panel createTicketInputLayout() {
		ticketLineLayout.setSpacing(false);
		ticketLineLayout.setSizeFull();

		Button proofButton = new Button("Tickets prüfen");
		proofButton.setClickShortcut(KeyCode.ENTER);
		proofButton.setStyleName(ValoTheme.BUTTON_PRIMARY);
		proofButton.addClickListener(new Button.ClickListener() {

			public void buttonClick(Button.ClickEvent event) {
				List<String> ticketNumbers = new LinkedList<String>();
				for (Component component : ticketLineLayout) {
					HorizontalLayout ticketLine = (HorizontalLayout) component;
					TextField projectField = (TextField) ticketLine
							.getComponent(0);
					TextField numberField = (TextField) ticketLine
							.getComponent(1);
					ticketNumbers.addAll(TicketStringCreator.createTicketList(
							projectField.getValue(), numberField.getValue()));
				}
				Results results = jira.proofTickets(ticketNumbers);
				String statistics = String.format(
						"<b>%d %s</b><br><b>%d %s</b><br><b>%d %s</b> ",
						results.getCountGoodTickets(), Status.GOOD.toString(),
						results.getCountUknownTickets(),
						Status.UKNOWN.toString(), results.getCountBadTickets(),
						Status.BAD.toString());
				statisticLabel.setValue(statistics);
				statisticLabel.setContentMode(ContentMode.HTML);
				ticketContainer.removeAllItems();
				ticketContainer.addAll(results.getTicketResults());
			}
		});
		HorizontalLayout buttonLayout = new HorizontalLayout(proofButton);
		buttonLayout.setSpacing(true);
		buttonLayout.setMargin(new MarginInfo(true, false, true, false));

		ticketLineLayout.addComponent(createTicketInputLine());
		final VerticalLayout l = new VerticalLayout(ticketLineLayout,
				buttonLayout);
		l.setMargin(new MarginInfo(false, true, false, true));
		Panel ticketInputPanel = new Panel("GoodBadTicket");
		ticketInputPanel.setContent(l);

		return ticketInputPanel;
	}

	private Component createTicketInputLine() {
		TextField projectTextField = new TextField("Jira-Projekt");
		projectTextField.setInputPrompt("z.B. STR");

		TextField ticketNumberTextField = new TextField("Ticketnummern");
		ticketNumberTextField
				.setInputPrompt("z.B. 124, 158 160 und/oder 165-170 ...");
		ticketNumberTextField.setSizeFull();

		final HorizontalLayout ticketInputLayout = new HorizontalLayout();
		ticketInputLayout.setSizeFull();
		ticketInputLayout.setSpacing(true);
		ticketInputLayout.addComponent(projectTextField);
		ticketInputLayout.addComponent(ticketNumberTextField);
		ticketInputLayout.setExpandRatio(ticketNumberTextField, 1);

		final int currentProjectIndex = ticketLineLayout.getComponentCount();
		if (currentProjectIndex > 0) {
			Button deleteButton = new Button("-");
			deleteButton.addClickListener(new Button.ClickListener() {
				public void buttonClick(Button.ClickEvent event) {
					ticketLineLayout.removeComponent(ticketInputLayout);
				}
			});
			ticketInputLayout.addComponent(deleteButton);
			ticketInputLayout.setComponentAlignment(deleteButton,
					Alignment.BOTTOM_RIGHT);
		} else {
			Button addButton = new Button("+");
			addButton.addClickListener(new Button.ClickListener() {

				public void buttonClick(Button.ClickEvent event) {
					ticketLineLayout.addComponent(createTicketInputLine());
				}
			});
			ticketInputLayout.addComponent(addButton);
			ticketInputLayout.setComponentAlignment(addButton,
					Alignment.BOTTOM_RIGHT);
		}

		return ticketInputLayout;
	}

}
