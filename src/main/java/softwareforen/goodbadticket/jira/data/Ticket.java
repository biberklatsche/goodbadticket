package softwareforen.goodbadticket.jira.data;

import com.atlassian.jira.rest.client.domain.Issue;
import java.io.Serializable;

/**
 * 
 * @author lars-wolfram
 */
public class Ticket implements Serializable {
	private static final long serialVersionUID = -1520923107014804137L;

	private TicketResult result;
	private Issue jiraTicket;

	public static TicketResult unknownTicket(String ticketNumber) {
		TicketResult result = new TicketResult(ticketNumber);
		result.addErrorDescription("Ticket nicht gefunden.");
		result.setStatus(Status.NONE);
		return result;
	}

	public Ticket(String ticketNumber, Issue jiraTicket, String baseUrl) {
		this.jiraTicket = jiraTicket;
		TicketResult ticketResult = new TicketResult(ticketNumber);
		ticketResult.setLink(baseUrl + "/browse/" + ticketNumber);
		this.result = ticketResult;
	}

	public Issue getJiraTicket() {
		return jiraTicket;
	}

	public TicketResult getResult() {
		return result;
	}

	public Integer getEstimateInMinutes() {
		return jiraTicket.getTimeTracking().getOriginalEstimateMinutes() != null ? jiraTicket
				.getTimeTracking().getOriginalEstimateMinutes() : 0;
	}

	public Integer getTimeSpentInMinutes() {
		return jiraTicket.getTimeTracking().getTimeSpentMinutes() != null ? jiraTicket
				.getTimeTracking().getTimeSpentMinutes() : 0;
	}

	public void setResultStatus(Status status) {
		this.result.setStatus(status);

	}

	public void addResultErrorDescription(String description) {
		this.result.addErrorDescription(description);
	}

	public String getDescription() {
		return this.jiraTicket.getDescription();
	}

	public String getJiraTicketStatus() {
		if (this.jiraTicket.getStatus() != null
				&& this.jiraTicket.getStatus().getName() != null) {
			return jiraTicket.getStatus().getName();
		}
		return "";
	}

	public boolean hasVersion() {
		if (this.jiraTicket.getFixVersions() != null) {
			return this.jiraTicket.getFixVersions().iterator().hasNext();
		}
		return false;
	}
}