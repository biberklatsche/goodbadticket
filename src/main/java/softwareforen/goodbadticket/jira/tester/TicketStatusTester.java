package softwareforen.goodbadticket.jira.tester;

import softwareforen.goodbadticket.jira.data.Status;
import softwareforen.goodbadticket.jira.data.Ticket;

public final class TicketStatusTester {

	private TicketStatusTester() {
	}

	public static Ticket test(Ticket ticket) {
		if (ticket.getJiraTicketStatus().equalsIgnoreCase("fertig")) {
			ticket.setResultStatus(Status.GOOD);
		} else {
			ticket.setResultStatus(Status.BAD);
			ticket.addResultErrorDescription("Ticket nicht abgeschlossen.");
		}
		return ticket;
	}
}
