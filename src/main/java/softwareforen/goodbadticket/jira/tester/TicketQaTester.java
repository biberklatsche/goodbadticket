package softwareforen.goodbadticket.jira.tester;

import com.atlassian.jira.rest.client.domain.Comment;
import softwareforen.goodbadticket.jira.data.Status;
import softwareforen.goodbadticket.jira.data.Ticket;

public final class TicketQaTester {
	private TicketQaTester() {
	}

	public static Ticket test(Ticket ticket) {
		Iterable<Comment> comments = ticket.getJiraTicket().getComments();
		boolean qaFound = false;
		for (Comment comment : comments) {
			String body = comment.getBody().toLowerCase().replaceAll("\\s", "");
			if (body.contains("qaerfolgreich")) {
				qaFound = true;
			}
			if (body.contains("keineqa")) {
				qaFound = true;
			}
			if (body.contains("qanichterforderlich")) {
				qaFound = true;
			}
			if (body.contains("qamit") || body.contains("qaam")) {
				qaFound = true;
			}
			if (body.contains("qazusammenmit")) {
				qaFound = true;
			}
		}
		if (qaFound) {
			ticket.setResultStatus(Status.GOOD);
		} else {
			ticket.setResultStatus(Status.UKNOWN);
			ticket.addResultErrorDescription("Keine QA gefunden.");
		}
		return ticket;
	}
}
