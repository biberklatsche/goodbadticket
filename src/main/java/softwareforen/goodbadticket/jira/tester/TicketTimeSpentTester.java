package softwareforen.goodbadticket.jira.tester;

import softwareforen.goodbadticket.jira.data.Status;
import softwareforen.goodbadticket.jira.data.Ticket;

public final class TicketTimeSpentTester {

	private TicketTimeSpentTester() {
	}

	public static Ticket test(Ticket ticket) {
		if (ticket.getTimeSpentInMinutes() == 0) {
			ticket.setResultStatus(Status.BAD);
			ticket.addResultErrorDescription("Keine Zeit gebucht.");
		} else if (ticket.getEstimateInMinutes() * 1.2 >= ticket
				.getTimeSpentInMinutes()) {
			ticket.setResultStatus(Status.GOOD);
		} else if (ticket.getEstimateInMinutes() == 0) {
			ticket.setResultStatus(Status.GOOD);
		} else {
			ticket.setResultStatus(Status.BAD);
			ticket.addResultErrorDescription("Mehr als 20% über der Zeit.");
		}
		return ticket;
	}
}
