package softwareforen.goodbadticket.jira.tester;

import com.atlassian.jira.rest.client.domain.Comment;
import softwareforen.goodbadticket.jira.data.Status;
import softwareforen.goodbadticket.jira.data.Ticket;

public final class TicketQaDescriptionTester {

	private TicketQaDescriptionTester() {
	}

	public static Ticket test(Ticket ticket) {
		Iterable<Comment> comments = ticket.getJiraTicket().getComments();
		boolean qaDescriptionFound = false;
		for (Comment comment : comments) {
			String body = comment.getBody().toLowerCase().replaceAll("\\s", "");
			if (body.contains("fürqa")) {
				qaDescriptionFound = true;
			}
			if (body.contains("qa:") && !body.contains("qaerfolgreich")) {
				qaDescriptionFound = true;
			}
			if (body.contains("keine qa")) {
				qaDescriptionFound = true;
			}
			if (body.contains("qanichterforderlich")) {
				qaDescriptionFound = true;
			}
			if (body.contains("qa") && !body.contains("qaerfolgreich")) {
				qaDescriptionFound = true;
			}
			if (body.contains("hinweisqa")) {
				qaDescriptionFound = true;
			}
			if (body.contains("qahinweis")) {
				qaDescriptionFound = true;
			}
			if (body.contains("qa-todo")) {
				qaDescriptionFound = true;
			}
			if (body.contains("qatodo")) {
				qaDescriptionFound = true;
			}
			if (body.contains("fürdieqa")) {
				qaDescriptionFound = true;
			}
			if (body.contains("qamit") || body.contains("qaam")) {
				qaDescriptionFound = true;
			}
		}
		if (qaDescriptionFound) {
			ticket.setResultStatus(Status.GOOD);
		} else {
			ticket.setResultStatus(Status.UKNOWN);
			ticket.addResultErrorDescription("Keine QA Beschreibung gefunden.");
		}
		return ticket;
	}
}
