package softwareforen.goodbadticket.jira.tester;

import softwareforen.goodbadticket.jira.data.Status;
import softwareforen.goodbadticket.jira.data.Ticket;

public final class TicketDescriptionTester {

	private TicketDescriptionTester() {
	}

	public static Ticket test(Ticket ticket) {
		if (ticket.getDescription() == null
				|| ticket.getDescription().isEmpty()) {
			ticket.setResultStatus(Status.GOOD);
			ticket.addResultErrorDescription("Keine Beschreibung am Ticket.");
		} else {
			ticket.setResultStatus(Status.GOOD);
		}
		return ticket;
	}
}
