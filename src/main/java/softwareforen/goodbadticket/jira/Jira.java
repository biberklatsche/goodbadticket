/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package softwareforen.goodbadticket.jira;

import softwareforen.goodbadticket.jira.data.Results;
import softwareforen.goodbadticket.jira.data.Ticket;
import softwareforen.goodbadticket.jira.tester.TicketQaTester;
import softwareforen.goodbadticket.jira.tester.TicketQaDescriptionTester;
import softwareforen.goodbadticket.jira.tester.TicketStatusTester;
import softwareforen.goodbadticket.jira.tester.TicketTimeSpentTester;
import softwareforen.goodbadticket.jira.tester.TicketTimeEstimateTester;
import softwareforen.goodbadticket.jira.tester.TicketVersionTester;
import softwareforen.goodbadticket.jira.tester.TicketDescriptionTester;
import com.atlassian.jira.rest.client.IssueRestClient;
import com.atlassian.jira.rest.client.JiraRestClient;
import com.atlassian.jira.rest.client.JiraRestClientFactory;
import com.atlassian.jira.rest.client.domain.Issue;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import com.atlassian.util.concurrent.Promise;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author lars-wolfram
 */
public class Jira implements Serializable {

	private final static String JIRA_URL = "http://dev.softwareforen.de/jira";
	private final static String JIRA_ADMIN_USERNAME = "technical_ticket_check";
	private final static String JIRA_ADMIN_PASSWORD = "kMi53NP";
	private final IssueRestClient rClient;

	public Jira() throws URISyntaxException {
		JiraRestClientFactory factory = new AsynchronousJiraRestClientFactory();
		URI uri = new URI(JIRA_URL);
		JiraRestClient client = factory.createWithBasicHttpAuthentication(uri,
				JIRA_ADMIN_USERNAME, JIRA_ADMIN_PASSWORD);

		// Invoke the JRJC Client
		rClient = client.getIssueClient();
	}

	public Results proofTickets(List<String> ticketNumbers) {

		Results results = new Results();
		for (String ticketNumber : ticketNumbers) {
			try {
				Promise<Issue> pIssue = rClient.getIssue(ticketNumber);
				Issue issue = pIssue.get();
				if (issue != null) {
					Ticket ticket = new Ticket(ticketNumber, issue, JIRA_URL);
					ticket = TicketDescriptionTester.test(ticket);
					ticket = TicketStatusTester.test(ticket);
					ticket = TicketTimeEstimateTester.test(ticket);
					ticket = TicketTimeSpentTester.test(ticket);
					ticket = TicketVersionTester.test(ticket);
					ticket = TicketQaTester.test(ticket);
					ticket = TicketQaDescriptionTester.test(ticket);
					results.addTicketResult(ticket.getResult());
				} else {
					results.addTicketResult(Ticket.unknownTicket(ticketNumber));
				}

			} catch (InterruptedException ex) {
				Logger.getLogger(Jira.class.getName()).log(Level.SEVERE, null,
						ex);
			} catch (ExecutionException ex) {
				results.addTicketResult(Ticket.unknownTicket(ticketNumber));
			}
		}
		return results;
	}
}
